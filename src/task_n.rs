use super::*;

pub(crate) fn task_YYY_a() -> Result<impl std::fmt::Debug> {
    get_file("data/YYY.txt").and_then(|s| Ok(()))
}

pub(crate) fn task_YYY_b() -> Result<impl std::fmt::Debug> {
    get_file("data/YYY.txt").and_then(|s| Ok(()))
}

fn get_file<P: AsRef<Path>>(path: P) -> Result<()> {
    fs::read_to_string(path)
        .map_err(|e| e.into())
        .and_then(|s| transform(&s))
}

fn transform(inp: &str) -> Result<()> {
    Ok(())
}

mod tests {
    use super::*;

    #[test]
    fn a() {
        unimplemented!()
    }

    #[test]
    fn b() {
        unimplemented!()
    }
}

