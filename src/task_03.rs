use std::borrow::BorrowMut;

use super::*;

type House = (i32, i32);
type Maneuver = (i32, i32);

struct SantaVisitation {
    real_current: House,
    robo_current: House,
    real_houses: HashSet<House>,
    robo_houses: HashSet<House>,
}

#[derive(Debug, PartialEq)]
enum Santa {
    Real,
    Robo,
}

impl SantaVisitation {
    fn new() -> Self {
        SantaVisitation {
            real_current: (0, 0),
            robo_current: (0, 0),
            real_houses: [(0, 0)].iter().cloned().collect(),
            robo_houses: [(0, 0)].iter().cloned().collect(),
        }
    }

    fn run(&mut self, maneuvers: &[Maneuver]) {
        for &man in maneuvers {
            self.real_current.0 += man.0;
            self.real_current.1 += man.1;
            self.real_houses
                .insert((self.real_current.0, self.real_current.1));
        }
    }

    fn run_alt(&mut self, maneuvers: &[Maneuver]) {
        let mut santa = Santa::Real;
        for &man in maneuvers {
            if santa == Santa::Real {
                self.real_current.0 += man.0;
                self.real_current.1 += man.1;
                self.real_houses
                    .insert((self.real_current.0, self.real_current.1));
                santa = Santa::Robo;
            } else {
                self.robo_current.0 += man.0;
                self.robo_current.1 += man.1;
                self.robo_houses
                    .insert((self.robo_current.0, self.robo_current.1));
                santa = Santa::Real;
            }
        }
    }

    fn count_visits(&self) -> usize {
        self.real_houses.len()
    }

    fn count_all_visits(&self) -> usize {
        self.real_houses.union(&self.robo_houses).collect::<HashSet<_>>().len()
    }
}

pub(crate) fn task_03_a() -> Result<impl std::fmt::Debug> {
    get_file("data/03.txt").and_then(|inp| {
        let mut s = SantaVisitation::new();
        s.run(&inp);
        Ok(s.count_visits())
    })
}

pub(crate) fn task_03_b() -> Result<impl std::fmt::Debug> {
    get_file("data/03.txt").and_then(|inp| {
        let mut s = SantaVisitation::new();
        s.run_alt(&inp);
        Ok(s.count_all_visits())
    })
}

fn get_file<P: AsRef<Path>>(path: P) -> Result<Vec<Maneuver>> {
    fs::read_to_string(path)
        .map_err(|e| e.into())
        .and_then(|s| transform(&s))
}

fn transform(s: &str) -> Result<Vec<Maneuver>> {
    s.trim()
        .lines()
        .flat_map(|line| {
            line.trim()
                .chars()
                .map(|c| match c {
                    '^' => Ok((0, 1)),
                    '>' => Ok((1, 0)),
                    '<' => Ok((-1, 0)),
                    'v' => Ok((0, -1)),
                    other @ _ => Err(AOCError::CharNotRecognized(other).into()),
                })
                .collect::<Vec<Result<Maneuver>>>()
        })
        .collect()
}

mod tests {
    use super::*;

    #[test]
    fn a() {
        assert_eq!(2, {
            let mut s = SantaVisitation::new();
            s.run(&transform(">").unwrap());
            s.count_visits()
        });
        assert_eq!(4, {
            let mut s = SantaVisitation::new();
            s.run(&transform("^>v<").unwrap());
            s.count_visits()
        });
        assert_eq!(2, {
            let mut s = SantaVisitation::new();
            s.run(&transform("^v^v^v^v^v").unwrap());
            s.count_visits()
        });
    }

    #[test]
    fn b() {
        assert_eq!(3, {
            let mut s = SantaVisitation::new();
            s.run_alt(&transform("^v").unwrap());
            s.count_all_visits()
        });
        assert_eq!(3, {
            let mut s = SantaVisitation::new();
            s.run_alt(&transform("^>v<").unwrap());
            s.count_all_visits()
        });
        assert_eq!(11, {
            let mut s = SantaVisitation::new();
            s.run_alt(&transform("^v^v^v^v^v").unwrap());
            s.count_all_visits()
        });
    }
}
