use std::cmp::max;

use super::*;

pub(crate) fn task_05_a() -> Result<impl std::fmt::Debug> {
    get_file("data/05.txt").and_then(|s| Ok(s.iter().filter(|&x| is_nice(x)).count()))
}

pub(crate) fn task_05_b() -> Result<impl std::fmt::Debug> {
    get_file("data/05.txt").and_then(|s| Ok(s.iter().filter(|&x| is_nice_p2(x)).count()))
}

fn get_file<P: AsRef<Path>>(path: P) -> Result<Vec<String>> {
    fs::read_to_string(path)
        .map_err(|e| e.into())
        .and_then(|s| transform(&s))
}

fn transform(inp: &str) -> Result<Vec<String>> {
    Ok(inp.trim().lines().map(|st| st.trim().to_string()).collect())
}

fn is_nice(inp: &str) -> bool {
    inp.chars()
        .zip(inp.chars().skip(1))
        .filter(|(a, b)| a == b)
        .count()
        > 0
        && inp.chars().filter(|&x| "aeiou".contains(x)).count() >= 3
        && !(inp
            .chars()
            .zip(inp.chars().skip(1))
            .filter(|&(a, b)| {
                (a == 'a' && b == 'b')
                    || (a == 'c' && b == 'd')
                    || (a == 'p' && b == 'q')
                    || (a == 'x' && b == 'y')
            })
            .count()
            > 0)
}

fn is_nice_p2(inp: &str) -> bool {
    inp.chars().count() > 3
        && inp
            .chars()
            .enumerate()
            .zip(inp.chars().skip(1))
            .filter(|((i, a), b)| inp.get(min(i + 2, inp.len())..).unwrap().contains(&format!("{}{}", a, b)))
            .count()
            > 0
        && inp
            .chars()
            .zip(inp.chars().skip(2))
            .filter(|(a, b)| a == b)
            .count()
            > 0
}

mod tests {
    use super::*;

    #[test]
    fn a() {
        assert_eq!(true, is_nice("ugknbfddgicrmopn"));
        assert_eq!(true, is_nice("aaa"));
        assert_eq!(false, is_nice("jchzalrnumimnmhp"));
        assert_eq!(false, is_nice("haegwjzuvuyypxyu"));
        assert_eq!(false, is_nice("dvszwmarrgswjxmb"));
    }

    #[test]
    fn b() {
        assert_eq!(true, is_nice_p2("qjhvhtzxzqqjkmpb"));
        assert_eq!(true, is_nice_p2("xxyxx"));
        assert_eq!(true, is_nice_p2("xyxy"));
        assert_eq!(false, is_nice_p2("aaa"));
        assert_eq!(false, is_nice_p2("uurcxstgmygtbstg"));
        assert_eq!(false, is_nice_p2("ieodomkazucvgmuy"));
    }
}
