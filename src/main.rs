#![deny(clippy::all)]
#![allow(unused_imports)]

#[cfg(feature = "backtraces")]
use std::backtrace::Backtrace;
use std::collections::{HashMap, HashSet};
use std::cmp::{min, max};
use std::fs;
use std::hash::{Hash, Hasher};
use std::iter::FromIterator;
use std::path::Path;
use std::str::FromStr;
use std::time::Instant;
use std::cell::RefCell;
use std::rc::Rc;

use anyhow::Result;
use logos::{Lexer, Logos};
use thiserror::Error;
use ::md5;
// use ::bytecount;
// use ::permute::permute;

#[macro_export]
macro_rules! import_and_use_mod {
    ($mod_name:ident) => {
        mod $mod_name;
        #[allow(unused_imports)]
        use crate::$mod_name::*;
    };
}

import_and_use_mod!(util);

import_and_use_mod!(task_01);
import_and_use_mod!(task_02);
import_and_use_mod!(task_03);
import_and_use_mod!(task_04);
import_and_use_mod!(task_05);
import_and_use_mod!(task_06);

#[allow(clippy::println_empty_string)]
fn main() -> Result<()> {
    println!("Disk I/O will be included in timers.");

    println!("");

    timer_create_run!(task_01_a);
    timer_create_run!(task_01_b);

    println!("");

    timer_create_run!(task_02_a);
    timer_create_run!(task_02_b);

    println!("");

    timer_create_run!(task_03_a);
    timer_create_run!(task_03_b);

    println!("");

    timer_create_run!(task_04_a);
    timer_create_run!(task_04_b);

    println!("");

    timer_create_run!(task_05_a);
    timer_create_run!(task_05_b);

    println!("");

    timer_create_run!(task_06_a);
    timer_create_run!(task_06_b);

    Ok(())
}
