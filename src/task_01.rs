use super::*;

pub(crate) fn task_01_a() -> Result<impl std::fmt::Debug> {
    get_file("data/01.txt").and_then(|s| Ok(calculate_floor(&s)))
}

pub(crate) fn task_01_b() -> Result<impl std::fmt::Debug> {
    get_file("data/01.txt").and_then(|s| Ok(get_first_basement_instruction(&s)))
}

fn get_first_basement_instruction(s: &[i64]) -> i64 {
    s.iter().zip(1i64..).fold((None, 0i64), |acc, (&x,i)| {
        if let Some(i) = acc.0 {
            (Some(i), acc.1)
        } else {
            match acc.1 + x {
                a if a == -1 => (Some(i), a),
                a => (None, a),
            }
        }
    }).0.unwrap_or_default()
}

fn calculate_floor(s: &[i64]) -> i64 {
    s.into_iter().fold(0i64, |acc, x| {
        acc + x
    })
}

fn get_file<P: AsRef<Path>>(path: P) -> Result<Vec<i64>> {
    fs::read_to_string(path)
        .map_err(|e| e.into())
        .and_then(|s| Ok(transform(&s)))
}

fn transform(s: &str) -> Vec<i64> {
    s.chars().map(|c| match c {
        '(' => 1,
        ')' => -1,
        _ => 0,
    }).collect::<Vec<_>>()
}

mod tests {
    use super::*;

    #[test]
    fn a() {
        assert_eq!(calculate_floor(&transform("(())")), 0);
        assert_eq!(calculate_floor(&transform("()()")), 0);
        assert_eq!(calculate_floor(&transform("(((")), 3);
        assert_eq!(calculate_floor(&transform("(()(()(")), 3);
        assert_eq!(calculate_floor(&transform("))(((((")), 3);
        assert_eq!(calculate_floor(&transform("())")), -1);
        assert_eq!(calculate_floor(&transform("))(")), -1);
        assert_eq!(calculate_floor(&transform(")))")), -3);
        assert_eq!(calculate_floor(&transform(")())())")), -3);
    }

    #[test]
    fn b() {
        assert_eq!(get_first_basement_instruction(&transform(")")), 1);
        assert_eq!(get_first_basement_instruction(&transform("()())")), 5);
    }
}
