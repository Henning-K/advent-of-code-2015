use super::*;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
struct GiftBox {
    l: i64,
    w: i64,
    h: i64,
}

impl GiftBox {
    fn calc_wrapping_paper(&self) -> i64 {
        let (l, w, h) = (self.l, self.w, self.h);
        2 * l * w + 2 * w * h + 2 * h * l + [l * w, w * h, h * l].iter().min().unwrap_or(&0)
    }

    fn calc_ribbon_length(&self) -> i64 {
        let mut v= vec![self.l, self.w, self.h];
        v.sort();
        v[0]*2 + v[1]*2 + v.iter().product::<i64>()
    }
}

impl FromIterator<i64> for GiftBox {
    fn from_iter<T: IntoIterator<Item = i64>>(iter: T) -> Self {
        let mut iter = iter.into_iter();
        GiftBox {
            l: iter.next().unwrap_or_default(),
            w: iter.next().unwrap_or_default(),
            h: iter.next().unwrap_or_default(),
        }
    }
}

pub(crate) fn task_02_a() -> Result<impl std::fmt::Debug> {
    get_file("data/02.txt").and_then(|s| Ok(s.iter().map(GiftBox::calc_wrapping_paper).sum::<i64>()))
}

pub(crate) fn task_02_b() -> Result<impl std::fmt::Debug> {
    get_file("data/02.txt").and_then(|s| Ok(s.iter().map(GiftBox::calc_ribbon_length).sum::<i64>()))
}

fn get_file<P: AsRef<Path>>(path: P) -> Result<Vec<GiftBox>> {
    fs::read_to_string(path)
        .map_err(|e| e.into())
        .and_then(|s| transform(&s))
}

fn transform(s: &str) -> Result<Vec<GiftBox>> {
    s.trim()
        .lines()
        .map(|line| {
            line.trim()
                .split('x')
                .map(|c| i64::from_str(c).map_err(|e| e.into()))
                .collect::<Result<GiftBox>>()
        })
        .collect()
}

mod tests {
    use super::*;

    #[test]
    fn a() {
        assert_eq!(
            transform("2x3x4").and_then(|s| Ok(s.iter().map(GiftBox::calc_wrapping_paper).sum::<i64>())).unwrap(),
            58
        );
        assert_eq!(
            transform("1x1x10").and_then(|s| Ok(s.iter().map(GiftBox::calc_wrapping_paper).sum::<i64>())).unwrap(),
            43
        );
    }

    #[test]
    fn b() {
        assert_eq!(
            transform("2x3x4").and_then(|s| Ok(s.iter().map(GiftBox::calc_ribbon_length).sum::<i64>())).unwrap(),
            34
        );
        assert_eq!(
            transform("1x1x10").and_then(|s| Ok(s.iter().map(GiftBox::calc_ribbon_length).sum::<i64>())).unwrap(),
            14
        );
    }
}
