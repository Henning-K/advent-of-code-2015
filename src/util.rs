use super::*;

pub(crate) struct Timer<'a> {
    desc: &'a str,
}

impl<'a> Timer<'a> {
    pub(crate) fn create(desc: &'a str) -> Self {
        Timer { desc }
    }

    pub(crate) fn run<T: std::fmt::Debug, F: (Fn() -> Result<T>)>(&self, f: F) -> Result<()> {
        let start = Instant::now();
        let result = f()?;
        let dur = start.elapsed();

        println!(
            "{} = {:#?} done in {}{}",
            self.desc,
            result,
            if dur.as_secs() > 0 {
                format!("{:03}s", dur.as_secs())
            } else {
                String::new()
            },
            if dur.subsec_millis() > 0 {
                format!(
                    "{}.{:06} ms",
                    dur.subsec_millis(),
                    dur.subsec_nanos() % 1_000_000
                )
            } else if dur.subsec_micros() > 0 {
                format!(
                    "{}.{:03}µs",
                    dur.subsec_micros(),
                    dur.subsec_nanos() % 1_000
                )
            } else if dur.subsec_nanos() > 0 {
                format!("{}ns", dur.subsec_nanos())
            } else {
                String::new()
            }
        );
        Ok(())
    }
}

#[macro_export]
macro_rules! timer_create_run {
    ($task:ident) => {
        Timer::create(&stringify!($task).replace("task", "timer")).run($task)?;
    };
}

#[allow(dead_code)]
#[derive(Debug, Error)]
pub enum AOCError {
    #[error("Parsing number input failed.")]
    NumParsing(#[from] std::num::ParseIntError),
    #[error("Something went wrong with IO.")]
    IOError(#[from] std::io::Error),
    #[error("Character '{0:?}' not recognized")]
    CharNotRecognized(char),
    #[error("Something went wrong.")]
    GenericError,
}
