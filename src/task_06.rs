use std::num::ParseIntError;

use super::*;

pub(crate) fn task_06_a() -> Result<impl std::fmt::Debug> {
    get_file("data/06.txt").and_then(|lines| {
        Ok(build_light_matrix(&lines, false).values().sum::<u32>())
    })
}

pub(crate) fn task_06_b() -> Result<impl std::fmt::Debug> {
    get_file("data/06.txt").and_then(|lines|  {
        Ok(build_light_matrix(&lines, true).values().sum::<u32>())
    })
}

fn get_file<P: AsRef<Path>>(path: P) -> Result<Vec<Line>> {
    fs::read_to_string(path)
        .map_err(|e| e.into())
        .and_then(|s| transform(&s))
}

fn transform(inp: &str) -> Result<Vec<Line>> {
    Ok(inp.trim().lines().map(|st| parse_line(st.trim())).collect())
}

fn build_light_matrix(line: &[Line], part_b: bool) -> LightMatrix {
    line.into_iter().fold(HashMap::new(), |mut acc, light_action| {
        for x in light_action.1.0 ..= light_action.2.0 {
            for y in light_action.1.1 ..= light_action.2.1 {
                let light = acc.entry((x,y)).or_insert(0);
                match (light_action.0.clone(), part_b) {
                    (LightAction::On, false)        => { *light = 1;  },
                    (LightAction::Off, false)       => { *light = 0; },
                    (LightAction::Toggle, false)    => { *light ^= 1; },
                    (LightAction::On, true)         => { *light += 1;  },
                    (LightAction::Off, true)        => { *light = (*light).saturating_sub(1); },
                    (LightAction::Toggle, true)     => { *light += 2; },
                }
            }
        }
        acc
    })
}

type LightMatrix = HashMap<(u32,u32), u32>;
type Line = (LightAction, Coordinate, Coordinate);

#[derive(Debug, PartialEq)]
struct Coordinate(u32,u32);

impl From<(u32,u32)> for Coordinate {
    fn from(tup: (u32,u32)) -> Self {
        Coordinate(tup.0, tup.1)
    }
}

impl FromStr for Coordinate {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let coords: Result<Vec<_>, Self::Err> = s.trim().split(",").map(u32::from_str).collect();
        coords.map(|v| (v[0], v[1]).into())
    }
}

#[derive(Logos, Debug, PartialEq)]
enum LightToken {
    #[regex(r"[ \t\n\f]+", logos::skip)]
    #[error]
    Error,

    #[token("toggle")]
    Toggle,
    
    #[token("turn on")]
    TurnOn,
    
    #[token("turn off")]
    TurnOff,

    #[token("through")]
    Separator,
    
    #[regex("[0-9]+,[0-9]+", |lex| Coordinate::from_str(lex.slice()))]
    Coord(Coordinate),
}

#[derive(Debug, PartialEq,Clone, Copy)]
enum LightAction {
    On,
    Off,
    Toggle,
}

fn parse_line(inp: &str) -> (LightAction, Coordinate, Coordinate) {
    let mut lex  = LightToken::lexer(inp);

    let action = match lex.next() {
        Some(LightToken::Toggle) => LightAction::Toggle,
        Some(LightToken::TurnOff) => LightAction::Off,
        Some(LightToken::TurnOn) => LightAction::On,
        _ => unreachable!(),
    };

    let first_coord = match lex.next() {
        Some(LightToken::Coord(coord)) => coord,
        _ => unreachable!(),
    };

    let _sep = match lex.next() {
        Some(LightToken::Separator) => (),
        _ => unreachable!(),
    };

    let second_coord = match lex.next() {
        Some(LightToken::Coord(coord)) => coord,
        _ => unreachable!(),
    };

    (action, first_coord, second_coord)
}

mod tests {
    use super::*;

    #[test]
    fn a() {
        let lines = transform(r#"turn on 0,0 through 999,999
        toggle 0,0 through 999,0
        turn off 499,499 through 500,500"#).unwrap();
        assert_eq!(build_light_matrix(&lines[0..1], false).values().sum::<u32>(), 1_000_000);
        assert_eq!(build_light_matrix(&lines[0..2], false).values().sum::<u32>(), 999_000);
        assert_eq!(build_light_matrix(&lines, false).values().sum::<u32>(), 998_996);
    }

    #[test]
    fn b() {
        let lines = transform(r#"turn on 0,0 through 0,0
        toggle 0,0 through 999,999"#).unwrap();
        assert_eq!(build_light_matrix(&lines[0..1], true).values().sum::<u32>(), 1);
        assert_eq!(build_light_matrix(&lines, true).values().sum::<u32>(), 2_000_001);
    }
}

