use core::fmt;

use super::*;

pub(crate) fn task_04_a() -> Result<impl fmt::Debug> {
    get_file("data/04.txt").and_then(|s| Ok(find_valid_hash(&s, "00000")))
}

pub(crate) fn task_04_b() -> Result<impl fmt::Debug> {
    get_file("data/04.txt").and_then(|s| Ok(find_valid_hash(&s, "000000")))
}

fn get_file<P: AsRef<Path>>(path: P) -> Result<String> {
    fs::read_to_string(path)
        .map_err(|e| e.into())
        .and_then(|s| transform(&s))
}

fn transform(inp: &str) -> Result<String> {
    Ok(inp.trim().lines().flat_map(|s| s.trim().chars()).collect())
}

fn find_valid_hash(secret_key: &str, prefix: &str) -> u32 {
    (0u32..).map(|n| {
        (
            format!("{:x}", md5::compute(format!("{}{}", secret_key, n).as_bytes())),
            n,
        )
    }).filter(|(hash, _n)| {
        hash.starts_with(prefix)
    }).nth(0).unwrap().1
}

mod tests {
    use super::*;

    #[test]
    fn a() {
        let (secret_key, result) = ("abcdef", 609043);
        assert_eq!(find_valid_hash(secret_key, "00000"), result);
        let (secret_key, result) = ("pqrstuv", 1048970);
        assert_eq!(find_valid_hash(secret_key, "00000"), result);
    }

    #[test]
    fn b() {
        // No test cases given.
    }
}
